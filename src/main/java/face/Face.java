package face;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@AllArgsConstructor
@Data
public class Face {
   private List<Vector> vectors;
   public Face() {
       this.vectors = new ArrayList<Vector>();
   }

   public void addVector(Vector ... vectors) {
       Stream.of(vectors).forEach((vector) -> this.vectors.add(vector));
   }
   public void addVector(Vector vector) {
       this.vectors.add(vector);
   }
   public static class Calculator {
       public static double getResult(Face face1, Face face2) {
           double result = 0;
           for (int i = 0; i < face1.getVectors().size(); i++) {
               result += Vector.Calculator.getEuclidean(
                       face1.getVectors().get(i),
                       face2.getVectors().get(i)
               );
           }
           return result/face1.getVectors().size();
       }
   }

}
