package face;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import  static  java.lang.Math.*;

@Data
@ToString
public class Vector {
    private double ax;
    private double ay;
    public Vector(@NonNull Point p1, @NonNull Point p2){
        this.ax = p1.x - p2.x;
        this.ay = p1.y - p2.y;
    }

    public static class Calculator {
        public static double getEuclidean(Vector v1, Vector v2) {
            Point point = new Point(
                    v1.getAx() - v2.getAx(),
                    v1.getAy() - v2.getAy()
            );
            return  sqrt(pow(point.x,2) + pow(point.y, 2));
        }
        public static double[] getManhattan(Vector v1, Vector v2) {
            Point point = new Point(
                    v1.getAx() - v2.getAx(),
                    v1.getAy() - v2.getAy()
            );
            return new double[]{abs(point.y), abs(point.y)};
        }

    }
}

