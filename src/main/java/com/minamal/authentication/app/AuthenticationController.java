package com.minamal.authentication.app;

import face.Face;
import face.Point;
import face.Vector;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.*;
import utils.Utils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Stream;


@CrossOrigin
@RestController
public class AuthenticationController {
    private Face baseFace = new Face(Arrays.asList(
            new Vector(
                    new Point(10, 20),
                    new Point(20, 100)
            ),
            new Vector(
                    new Point(100, 40),
                    new Point(200, 0)
            )
    ));
    @Autowired
    private ResourceLoader loader;
    @RequestMapping(path = "/", method ={ RequestMethod.GET })
    public String home() {
        return  "This is an Authentication Application";
    }

    @RequestMapping(path = "/", method ={ RequestMethod.POST })
    public String authentication(@RequestBody String body) {
        String values[] = body.split(",");
        Face face = this.covertFace(values);
        return BigDecimal.valueOf(Face.Calculator.getResult(this.baseFace, face)).toString();
    }
    private Face covertFace(String[] values) {
        Face face = new Face();
        for (int i = 0; i < values.length; i+=4) {
            face.addVector(
                    new Vector(
                            new Point(s2d(values[i]), s2d(values[i + 1])),
                            new Point(s2d(values[i + 2]), s2d(values[i + 3]))
                    ));
        }

    }
    private double s2d(String str) {
        return Double.parseDouble(str);

    }
}