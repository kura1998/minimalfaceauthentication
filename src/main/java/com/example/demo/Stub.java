package com.example.demo;

import face.Point;
import face.Vector;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
public class Stub {
    @PostMapping("/token")
    public Vector authentication(HttpServletRequest request) throws Exception {
        BufferedReader bufferReaderBody = new BufferedReader(request.getReader());
        String[] body = bufferReaderBody.readLine().split(",");
        String id = body[0];
        double[] vectors = new double[body.length-1];

        for(int i = 1; i < vectors.length; i++){
            vectors[i] = Double.parseDouble(body[i]);
        }

        Vector vector = new Vector(
                new Point(vectors[0],vectors[1]),
                new Point(vectors[2],vectors[3])
        );
        return vector;
    }
}
